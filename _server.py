#!/usr/bin/env python 

""" 
An echo server that uses threads to handle multiple clients at a time. 
Entering any line of input at the terminal will exit the server. 
""" 

import socket 
import sys 
import threading 
import random as rd
import time as t
import numpy as np


def dot(x, y):
	"""
	2D dot product
	"""
	return x[0]*y[0] + x[1]*y[1]

def scale(x, a):
	"""
	2D scalar multiplication
	"""
	return (x[0]*a, x[1]*a)

def add(x, y):
	"""
	2D vector addition
	"""
	return (x[0]+y[0], x[1]+y[1])

class Ball(object):
	def __init__(self):
		"""
		Create a pong ball with the given inital position. 
		"""
		self.balldiameter = 40
		self.position = [(1100 - 40)/2.0, (600 - 40)/2.0]
		self.velocity = [0,0]

		self.Nhits = 0

	def reflect(self, surface, opt):
		"""
		Alter the ball's velocity for a perfectly elastic
		collision with a surface defined by the unit normal surface.
		
		surface: a tuple of floats
		"""
		diagonal = -2 * dot(surface, self.velocity)
		self.velocity = add(self.velocity, scale(surface, diagonal))

		if (opt == 'paddle'):
			
			self.Nhits = self.Nhits + 1

			if (surface[0] == -1):

				self.position = [1100 - 30 - self.balldiameter/2.0
				, self.position[1]]

			elif (surface[0] == 1):

				self.position = [30 + self.balldiameter/2.0, self.position[1]]

			if (self.Nhits % 5 == 0 and self.Nhits != 0):

			# increase ball velocity by 20 percent every five hits

				self.velocity = scale(self.velocity,1.2) 

	def hits_portal(self,teleportto):

		self.position = teleportto

	def reset(self):
		"""Reset the position, velocity and number of hits of the ball"""

		self.position = [(1100 - 40)/2.0, (600 - 40)/2.0]
		self.Nhits = 0

		self.velocity = (rd.choice([-1,1])*4,rd.choice([-1,1])*4)

class Portals(object):

	def __init__(self):

		self.cheight = 600
		self.cwidth = 1100

		self.portal_coords = [[30,0,self.cwidth/2.0 - 70,7],
								[self.cwidth/2.0 + 70,0,self.cwidth - 30,7],
								[30,self.cheight ,self.cwidth/2.0 - 70,self.cheight - 8 ],
								[self.cwidth/2.0 + 70, self.cheight ,self.cwidth - 30, self.cheight -8]]
		self.portal_yin = [7 + 20,7 + 20,self.cheight - 8 - 20,self.cheight -8 - 20]

		self.portalnum = [0,1]

	def randomize(self):

		self.portalnum = rd.sample(set([0,1,2,3]), 2)

class Brickwall(object):

	def __init__(self):

		self.n_bricks = 0
		self.bricksleft = self.n_bricks
		self.brick_width = 30
		self.x_brick = 1100/2.0 - self.brick_width/2.0
		self.y_brick = []
		self.hit_brick_num = "none"

	def randomize(self):

		self.n_bricks = rd.choice([2,3,4,5])
		self.bricksleft = self.n_bricks
		self.brick_height = 600/self.n_bricks
		self.hit_brick_num = "none"
		self.y_brick = []

		for i in range(self.n_bricks):

			self.y_brick.append([i*self.brick_height, i*self.brick_height + self.brick_height])

	def hit_brick_num_func(self,a):
		# find index of self.y_brick that contains a
		index = -1

		for i in range(len(self.y_brick)):
			if a >= self.y_brick[i][0] and a <= self.y_brick[i][1]:
				index = i
				break

		return index

class Server: 
	def __init__(self): 
		self.host = '' 
		self.port = 50000 
		self.backlog = 5 
		self.size = 1024 
		self.server = None 
		self.threads = [] 
		# the initialized variables below will be passed to
		# both clients
		self.paddlepos = [225,225]
		self.score = [0,0]
		self.ballc = Ball()
		self.portal = Portals()
		self.brickwall = Brickwall()
		self.obstacleactive = [0,0]
		self.obstacle_choice_init = [1,2]

	def open_socket(self): 
		try: 
			self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
			self.server.bind((self.host,self.port)) 
			self.server.listen(5)
			print 'make sure IP address is set to following: ', socket.gethostbyname(socket.gethostname())

		except socket.error, (value,message): 
			if self.server: 
				self.server.close() 
			print "Could not open socket: " + message 
			sys.exit(1) 

	def run(self): 
		self.open_socket() 

		running = 1 
		while running: 

			# handle the server socket 
			c = Client(self.server.accept(),len(self.threads)+1,
				self.paddlepos,self.ballc,self.score, self.obstacleactive, 
				self.obstacle_choice_init,self.portal,self.brickwall) 
			c.start() 
			self.threads.append(c) 

			if len(self.threads) == 1:

				print 'waiting for other player'

			if len(self.threads) == 2:

				print 'ready to launch'

				t.sleep(6)

				# wait for 6 seconds before starting count down

				for i in self.threads:

					i._ballc.velocity = (rd.choice([-1,1])*4,rd.choice([-1,1])*4)

		# close all threads 

		self.server.close() 
		for c in self.threads: 
			c.join() 

class Client(threading.Thread): 
	def __init__(self,(client,address),nPlayers,paddlepos,ballc,score,obsactive,obschoiceinit,portal,brickwall): 
		threading.Thread.__init__(self) 
		self.client = client 
		self.address = address 
		self.size = 1024 
		
		self._score = score
		self._nPlayers = nPlayers
		self._paddlepos = paddlepos
		self._ballc = ballc
		self._obsactive = obsactive
		self._obschoiceinit = obschoiceinit
		self.obstacle_choice = obschoiceinit[:]
		self._portal = portal
		self._brickwall = brickwall

		self.events = [self.hits_wall,
						self.hits_paddle,
						self.hits_portal,
						self.hits_brick_wall]

		self.responses = [lambda: ballc.reflect((0,self.direction),'wall'),
		lambda: ballc.reflect((self.direction,0),'paddle'),
		lambda: ballc.hits_portal(self.teleportto),
		lambda: ballc.reflect((self.direction,0),'brickwall')]

	def run(self): 

		running = 1 
		while running: 
			dataget = self.client.recv(self.size)
			if dataget != 'init':
				dataget = dataget.split()

				self._ballc.position = add(self._ballc.position, self._ballc.velocity)
				self._paddlepos[int(dataget[0])-1] = float(dataget[1])

				self.win() # check if someone wins
				

				for event, response in zip(self.events, self.responses):
					if event():
						response()

			"""
			datasend = 
			+ number of players
			+ ball position x
			+ ball position y
			+ ball velocity in x
			+ ball velocity in y
			+ first paddle y
			+ second paddle y
			+ score 1
			+ score 2
			+ portal state
			+ brickwall state
			"""

			datasend = str(self._nPlayers) + ' '\
			+ str(self._ballc.position[0]) + ' ' \
			+ str(self._ballc.position[1]) + ' ' \
			+ str(self._ballc.velocity[0]) + ' ' \
			+ str(self._ballc.velocity[1]) + ' ' \
			+ str(self._paddlepos[0]) + ' ' \
			+ str(self._paddlepos[1]) + ' ' \
			+ str(self._score[0]) + ' ' \
			+ str(self._score[1]) + ' ' \
			+ str (self._obsactive[0]) + '-' + str(self._portal.portalnum[0]) + '-' + str(self._portal.portalnum[1]) + ' ' \
			+ str (self._obsactive[1]) + '-' + str(self._brickwall.n_bricks) + '-' + str(self._brickwall.hit_brick_num)  + '-' + str(self._brickwall.bricksleft) 

			self.client.send(datasend)

	def hits_wall(self):

		upper_wall = self._ballc.position[1] < self._ballc.balldiameter/2.0

		lower_wall = self._ballc.position[1] > 600 - self._ballc.balldiameter/2.0

		if (upper_wall):

			self.direction = 1

		elif (lower_wall):

			self.direction = -1

		return (upper_wall or lower_wall)

	def hits_paddle(self):
		
		hits_left_paddle = (self._ballc.position[1] + self._ballc.balldiameter/2.0 > self._paddlepos[0]
			and self._ballc.position[1] - self._ballc.balldiameter/2.0 < self._paddlepos[0] + 150 
			and self._ballc.position[0] - self._ballc.balldiameter/2.0 < 30)

		hits_right_paddle = (self._ballc.position[1] + self._ballc.balldiameter/2.0 > self._paddlepos[1] 
			and self._ballc.position[1] - self._ballc.balldiameter/2.0 < self._paddlepos[1] + 150
			and self._ballc.position[0] + self._ballc.balldiameter/2.0 > 1070)

		if (hits_left_paddle):

			self.direction = 1
		
		elif(hits_right_paddle):

			self.direction = -1

		if(hits_left_paddle or hits_right_paddle):

			if self.obstacle_choice == []:
				self.obstacle_choice = self._obschoiceinit[:]

			thechoice = rd.choice(self.obstacle_choice)
			self.obstacle_choice.remove(thechoice)

			if (thechoice == 1):

				if self._obsactive[thechoice - 1] == 0:
					# create portal
					self._portal.randomize()
					self._obsactive[thechoice - 1] = 1
					
					
				else:
					# delete portal
					self._obsactive[thechoice - 1] = 0

			if (thechoice == 2):

				if self._brickwall.bricksleft == 0:
					self._obsactive[thechoice - 1] = 1
					self._brickwall.randomize()

			else:

				self._obsactive[1] == 0

		return (hits_left_paddle or hits_right_paddle)

	def hits_portal(self):

		portal1=0
		portal2=0

		if self._obsactive[0] == 1:

			yrange1 = [self._portal.portal_coords[self._portal.portalnum[0]][1], 
			self._portal.portal_coords[self._portal.portalnum[0]][3]]
			yrange1.sort()

			yrange2 = [self._portal.portal_coords[self._portal.portalnum[1]][1], 
			self._portal.portal_coords[self._portal.portalnum[1]][3]]
			yrange2.sort()

			ballrightx = self._ballc.position[0] + self._ballc.balldiameter/2.0
			ballleftx = self._ballc.position[0] - self._ballc.balldiameter/2.0
			ballymax = self._ballc.position[1] + self._ballc.balldiameter/2.0
			ballymin= self._ballc.position[1] - self._ballc.balldiameter/2.0

			portal1 = ((ballrightx>self._portal.portal_coords[self._portal.portalnum[0]][0]) 
				and (ballleftx<self._portal.portal_coords[self._portal.portalnum[0]][2]) 
				and (yrange1[0]<ballymax<yrange1[1] or yrange1[0]<ballymin<yrange1[1]))

			portal2 = ((ballrightx>self._portal.portal_coords[self._portal.portalnum[1]][0]) 
				and (ballleftx<self._portal.portal_coords[self._portal.portalnum[1]][2]) 
				and (yrange2[0]<ballymax<yrange2[1] or yrange2[0]<ballymin<yrange2[1]))

			if portal1:

				self.teleportto = [self._portal.portal_coords[self._portal.portalnum[1]][0] -
				(self._portal.portal_coords[self._portal.portalnum[0]][0] - self._ballc.position[0]),
				self._portal.portal_yin[self._portal.portalnum[1]]]
		
			if portal2:

				self.teleportto = [self._portal.portal_coords[self._portal.portalnum[0]][0] - 
				(self._portal.portal_coords[self._portal.portalnum[1]][0] - self._ballc.position[0]),
				self._portal.portal_yin[self._portal.portalnum[0]]]

			if (portal1 or portal2):

				if self._portal.portal_yin[self._portal.portalnum[0]] == self._portal.portal_yin[self._portal.portalnum[1]] ==27 :

					self._ballc.reflect((0,-1),'portal')

				if self._portal.portal_yin[self._portal.portalnum[0]] == self._portal.portal_yin[self._portal.portalnum[1]] == 572 :

					self._ballc.reflect((0,1),'portal')

		return(portal1 or portal2)

	def hits_brick_wall(self):

		if self._brickwall.bricksleft == 0:

			from_left = from_right = 0

		else: 

			from_left = (self._ballc.position[0] + self._ballc.balldiameter/2.0 > self._brickwall.x_brick #- 5 
				and self._ballc.position[0] + self._ballc.balldiameter/2.0 < self._brickwall.x_brick + self._brickwall.brick_width
				and self._brickwall.hit_brick_num_func(self._ballc.position[1]) != -1)

			from_right = (self._ballc.position[0] - self._ballc.balldiameter/2.0 < self._brickwall.x_brick + self._brickwall.brick_width #+ 5
				and self._ballc.position[0] - self._ballc.balldiameter/2.0 > self._brickwall.x_brick
				and self._brickwall.hit_brick_num_func(self._ballc.position[1]) != -1)

			if (from_left):

				self.direction = -1

			if (from_right):

				self.direction = 1

			if (from_left or from_right):

				# hit brick

				self._brickwall.hit_brick_num = self._brickwall.hit_brick_num_func(self._ballc.position[1])

				print 'hit brick', self._brickwall.hit_brick_num

				self._brickwall.bricksleft = self._brickwall.bricksleft - 1					

				self._brickwall.y_brick[self._brickwall.hit_brick_num] = [-1,-1]

		return (from_left or from_right)


	def win(self):

		left_win = ((self._ballc.position[1] + self._ballc.balldiameter/2.0< self._paddlepos[1] 
			or self._ballc.position[1] - self._ballc.balldiameter/2.0>self._paddlepos[1] + 150)
			and self._ballc.position[0] + self._ballc.balldiameter/2.0 > 1100)

		right_win = ((self._ballc.position[1] + self._ballc.balldiameter/2.0<self._paddlepos[0] 
			or self._ballc.position[1] - self._ballc.balldiameter/2.0>self._paddlepos[0] + 150)
			and self._ballc.position[0] - self._ballc.balldiameter/2.0 < 0)

		if (left_win == 1):

			self._score[0] = self._score[0] + 1

			self._brickwall.hit_brick_num = "all"
			self._brickwall.bricksleft = 0
			self._brickwall.n_bricks = 0
			self._brickwall.y_brick = []

			self._ballc.reset()

		elif(right_win == 1):

			self._score[1] = self._score[1] + 1

			self._brickwall.hit_brick_num = "all"
			self._brickwall.bricksleft = 0
			self._brickwall.n_bricks = 0
			self._brickwall.y_brick = []

			self._ballc.reset()

if __name__ == "__main__": 
	s = Server() 
	s.run()