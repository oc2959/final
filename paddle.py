

class Paddle(object):
	def __init__(self,root,_canvas,_paddle):
		"""
		Create a paddle with the given inital position. 
		Assign methods to it
		Bind Keys
		"""
		self.paddle_ = _paddle
		self.canvas_ = _canvas

		self.direction = 0
	
		self.paddleh = 150

		tol = 10
 
		def up(event):
			self.direction = -tol

		def down(event):
			self.direction = +tol

		def onkeyrelease(event):
			key = event.keysym
			
			if (str(key)=="Up" or str(key)=="Down"):
				self.direction = 0

		root.bind("<Up>",up)
		root.bind("<Down>",down)

		root.bind('<KeyRelease>', onkeyrelease)

	def paddle_constraint(self):

		"""prevent paddle from moving out of screen"""

		return not ((self.canvas_.coords(self.paddle_)[1] < 0 
			and self.direction<0) 
			or (self.canvas_.coords(self.paddle_)[1] + self.paddleh > 600
			and self.direction>0))


	def move(self):

		"""move paddle"""

		if (self.paddle_constraint()):
			self.canvas_.move(self.paddle_,0, self.direction)