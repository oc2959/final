
class Brickwall(object):

	def __init__(self,_canvas):

		self.cheight = float(_canvas['height'])
		self.cwidth = float(_canvas['width'])
		self.canvas_ = _canvas
		self.brick = [None]

		self.brick_width = 30
		self.x_brick = self.cwidth/2.0 - self.brick_width/2.0

	def create_bricks(self, num):

		if all(x is None for x in self.brick) and not num == 0:
			print 'create', num
			self.brick = []
			for i in range(num):
				self.brick_height = float(self.cheight)/num
				self.brick.append(self.canvas_.create_rectangle(self.x_brick ,i*self.brick_height ,self.x_brick + self.brick_width,
					i*self.brick_height + self.brick_height,fill='orange', outline = 'white'))

	def delete_brick(self,num):

		if num != "all" and num != "none":

			if self.brick[num] != None :

				print 'delete', num

				self.canvas_.delete(self.brick[num])
				self.brick[num] = None

	def delete_brick_wall(self,num):

		if num == "all" and not all(x is None for x in self.brick):

			print 'delete all'

			for i in range(len(self.brick)):

				self.canvas_.delete(self.brick[i])
				self.brick[i] = None




	