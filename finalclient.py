
import Tkinter as Tk
import socket
import sys
import time as t

import brickwall as bri
import paddle as pad
import portal as po


"""this code sets up the basic frame of the code including
the  canvas, ball, paddle, scores and buttons"""

class Application(Tk.Frame):

	def __init__(self, master=None):

		self.master_ = master
		Tk.Frame.__init__(self, self.master_)

		self.bkimage = Tk.PhotoImage(file="resources/bkg.gif")
		self.w = self.bkimage.width()
		h = self.bkimage.height()
		
		self.ballimg = Tk.PhotoImage(file="resources/ball.gif")
		self.ballw = self.ballimg.width()
		ballh = self.ballimg.height()

		self.paddle1img = Tk.PhotoImage(file="resources/paddle/w11.gif")
		self.paddle2img = Tk.PhotoImage(file="resources/paddle/w21.gif")
		paddlew = self.paddle1img.width()
		paddleh = self.paddle1img.height()

		master.resizable(width=False, height=False)
		master.title('Pong!')
		master.geometry("%dx%d+0+0" % (self.w, h))
		self.canvas = Tk.Canvas(master, width=self.w, height=h)
		self.canvas.pack(side='top', fill='both', expand='yes')
		self.canvas.create_image(0, 0, image=self.bkimage, anchor='nw')

		self.ball = self.canvas.create_image((self.w - self.ballw)/2.0 , (h - ballh)/2.0, image=self.ballimg, anchor='nw')

		self.paddleone = self.canvas.create_image(0, (h - paddleh)/2.0, image=self.paddle1img, anchor='nw')
		self.paddletwo = self.canvas.create_image(self.w, (h - paddleh)/2.0, image=self.paddle2img, anchor='ne')

		scorecolor = '#%02x%02x%02x' % (0, 220, 251)
		self.scoreone = self.canvas.create_text(self.w/2.0 - 20,0, text = '0',anchor='ne', fill = "red", font =("Helvetica", 30))
		self.scoretwo = self.canvas.create_text(self.w/2.0 + 20,0, text = '0',anchor='nw', fill = scorecolor, font =("Helvetica", 30))
		
		host = '192.168.1.70' #<----MAKE SURE YOU CHANGE THIS TO YOUR SERVER IP
		port = 50000
		self.size = 1024
		self.s = None		

		print 'Client: IP used is',host

		"""Updates the data"""

		try:
			self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			self.s.connect((host,port))
			print 'Client: Successful connection'

		except socket.error, (value, message):
			if self.s:
				self.s.close()
			print "Client: Could not open socket: " + message
			sys.exit(1)

		self.s.send('init') 

		data = self.s.recv(self.size)

		data = data.split()

		self.player = data[0]

		print 'You are player',self.player

		self.timer = self.canvas.create_text(self.w/2.0 - 30 ,30, text = '5',anchor='nw', fill = "red", font =("Helvetica", 100))

		self.ballposition = [self.canvas.coords(self.ball)[0] + self.ballw/2.0, 
		self.canvas.coords(self.ball)[1] + self.ballw/2.0]
		self.paddles = [self.paddleone,self.paddletwo]
		self.paddlec = pad.Paddle( master,self.canvas,self.paddles[int(self.player)-1])

		# obstacles

		self.portal = po.Portals(self.canvas)
		self.brickwall = bri.Brickwall(self.canvas)

	def connect_and_transfer(self):
		
		mypaddley = str(self.canvas.coords(self.paddlec.paddle_)[1])

		"""
		datasend = 
		+ player number
		+ y paddle position of current player
		+ ball position x (center)
		+ ball position y (center)
		"""
		datasend = self.player + ' ' + mypaddley 

		self.s.send(datasend) 

		data = self.s.recv(self.size)

		data = data.split()

		self.canvas.coords(self.paddles[[1,0][int(self.player)-1]], 
			[self.w,0][int(self.player)-1],
			data[[6,5][int(self.player)-1]])

		data[9] = data[9].replace('-', ' ').split(' ')
		data[10] = data[10].replace('-', ' ').split(' ')
		self.obstaclestate = [int(data[9][0]),int(data[10][0])]
	
		if data[10][2] != "all" and data[10][2] != "none":
			data[10][2] = int(data[10][2])

		# update variables with transfered data

		self.portalnum = [int(data[9][1]),int(data[9][2])]
		self.n_bricks = int(data[10][1])
		self.ballvel = [float(data[3]),float(data[4])]
		self.ballposition = [float(data[1]),float(data[2])]
		self.score = [int(data[7]),int(data[8])]
		self.brickhitnum = data[10][2]
		self.bricksleft = int(data[10][3])

	def pongloop(self):
		
		app.connect_and_transfer()

		if self.timer in self.canvas.find_all():

			if int(self.canvas.itemcget(self.timer, 'text')) > 0:

				if self.ballvel[0] != 0 and self.ballvel[1] != 0:

					t.sleep(1)
					self.canvas.itemconfig(self.timer, text=str(int(self.canvas.itemcget(self.timer, 'text'))-1))

			else:
				self.canvas.delete(self.timer)

		else:
			# timer has reached zero and is deleted
			# start game
			self.paddlec.move()

			self.canvas.coords(self.ball,self.ballposition[0] - self.ballw/2.0
				,self.ballposition[1] - self.ballw/2.0)

			self.canvas.itemconfig(self.scoreone, text=str(self.score[0]))
			self.canvas.itemconfig(self.scoretwo, text=str(self.score[1]))

			if self.obstaclestate[0] == 1:
				if len(self.portal.portal) == 0:
					self.portal.create_portal(self.portalnum)
			else:
				if len(self.portal.portal) != 0:
					self.portal.delete_portal(self.portalnum)

			if self.obstaclestate[1] == 1:

				if self.bricksleft == self.n_bricks:
					self.brickwall.create_bricks(self.n_bricks)
					
			self.brickwall.delete_brick(self.brickhitnum)
			self.brickwall.delete_brick_wall(self.brickhitnum)
		
		self.master_.after(5,self.pongloop)
		# reloop

if __name__ == "__main__": 
		
	root = Tk.Tk()
	app = Application(root)

	app.pongloop()

	root.mainloop()
	app.s.close()